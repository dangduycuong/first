//
//  FirstTests.swift
//  FirstTests
//
//  Created by Boss on 12/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import XCTest

@testable import First

class FirstTests: XCTestCase {
    
    func testBool() {
        let check: Bool = true
        XCTAssertTrue(check)
    }
    
    func testGetListAPI() {
        var checkStatus: Bool = false
        var components = URLComponents()
        components.queryItems = [URLQueryItem(name: "date", value: "2020-04-07")]
        //https://covid-19-statistics.p.rapidapi.com/reports/total?date=2020-04-07"
        components.scheme = "https"
        components.host = "covid-19-statistics.p.rapidapi.com"
        components.path = "/reports/total"
        
        var url = components.url
        
        var request = URLRequest(url: url!)
        request.timeoutInterval = 10
        request.cachePolicy = .useProtocolCachePolicy
        let headers = [
            "x-rapidapi-key": "caf4365e3cmsh9be42c339ac894ap178cadjsn8a3d333f17cc",
            "x-rapidapi-host": "covid-19-statistics.p.rapidapi.com"
        ]
        request.allHTTPHeaderFields = headers
        request.httpMethod = "GET"
        let _ = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            print("data",data)
            if let response = response {
                let httpResponse = response as! HTTPURLResponse
                let status = httpResponse.statusCode
                
                switch status {
                case 200:
                    checkStatus = true
                    print("Thanh cong")
                default:
                    checkStatus = false
                }
            }
            if (error != nil) {
            }
        }).resume()
        XCTAssertTrue(checkStatus)
    }
    
}
