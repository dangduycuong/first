//
//  MasterTabbarViewController.swift
//  First
//
//  Created by Boss on 14/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import UIKit
import Vision

class MasterTabbarViewController: UITabBarController {
    
    var user1 = "admin1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if user1 == "admin" {
            //Show All Tabs
        } else {
            tabBarController?.viewControllers?.remove(at: 1)
            let indexToRemove = 1
            if let tabBarController = self.tabBarController {
                
                if indexToRemove < tabBarController.viewControllers!.count {
                    var viewControllers = tabBarController.viewControllers
                    viewControllers?.remove(at: indexToRemove)
                    tabBarController.viewControllers = viewControllers
                }
            }
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    }
    
}
