//
//  DetailViewController.swift
//  First
//
//  Created by Boss on 12/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var nameImageView: UIImageView!
    
    var image: UIImage?
    var fileName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        callAPI()
        
        if let image = getSavedImage(named: fileName) {
            nameImageView.image = image
            self.image = image
        }
        
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            print("Name", named)
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    @IBAction func onTapPDF(_sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "PDFViewController") as! PDFViewController
        vc.pdfImage = image!
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func callAPI() {
        
        var components = URLComponents()
        components.queryItems = [URLQueryItem(name: "date", value: "2020-04-07")]
        //https://covid-19-statistics.p.rapidapi.com/reports/total?date=2020-04-07"
        components.scheme = "https"
        components.host = "covid-19-statistics.p.rapidapi.com"
        components.path = "/reports/total"
        
        var url = components.url
        
        var request = URLRequest(url: url!)
        request.timeoutInterval = 10
        request.cachePolicy = .useProtocolCachePolicy
        let headers = [
            "x-rapidapi-key": "caf4365e3cmsh9be42c339ac894ap178cadjsn8a3d333f17cc",
            "x-rapidapi-host": "covid-19-statistics.p.rapidapi.com"
        ]
        request.allHTTPHeaderFields = headers
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let response = response {
                let httpResponse = response as! HTTPURLResponse
                let status = httpResponse.statusCode
                
                switch status {
                case 200:
                    print("Thanh cong")
                break //print("Thanh cong")
                default:
                    break
                }
                
                if !(200...299).contains(httpResponse.statusCode) && !(httpResponse.statusCode == 304) {
                    if let httpError = error {// ... convert httpResponse.statusCode into a more readable error
                        //                        completion(.failure(httpError as! Error))
                        
                    }
                }
            }
            if (error != nil) {
                print(error)
            } else {
                if let json = data, let string = String(bytes: json, encoding: .utf8) {
                    print("json:\n", string)
                    
                }
            }
        })
        
        dataTask.resume()
    }
    
}

struct BaseModel: Codable {
    var data: SubData?
}

struct SubData: Codable {
    var last_update: String?
}


