//
//  UnitTest.swift
//  FirstTests
//
//  Created by Boss on 13/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import XCTest
import UIKit

@testable import First

class UnitTest: XCTestCase {

    func test() {
        let dataModel = DataModel(title: "1", name: "i", drinks: nil)
        
        let result = dataModel.checkDisplay()
        XCTAssertTrue(result)
        
    }

}
