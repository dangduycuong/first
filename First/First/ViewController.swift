//
//  ViewController.swift
//  First
//
//  Created by Boss on 12/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import UIKit

struct DataModel: Codable {
//    let title: String
//    let name: String
//
    var drinks: [DrinkModel]?
    
//    func checkDisplay() -> Bool {
//        if title == "1" {
//            return true
//        }
//        return false
//    }
    
}

struct DrinkModel: Codable {
    var strDrinkThumb: String?
}

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    let WIDTH_SCREEN = UIScreen.main.bounds.width
    // so item
    let numberOfItems: CGFloat = 4
    // khoang cach giua cac item
    let padding: CGFloat = 1
    
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var masterView: UIView!
    @IBOutlet weak var frameImageView: UIImageView!
    @IBOutlet weak var changeImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    var images1 = UIImage()
    
    var listCocktail = [DrinkModel]()
    var listImage = [UIImage]()
    var panGesture = UIPanGestureRecognizer()
    var checkStatus: Bool = false
    var checkCallAPi: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callAPIListImage()
        self.notificationLabel.isHidden = true
        
        var greeting = "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg"
        
//        let greeting = "Hello, world!"

        greeting.removeFirst(49)
    
//        if let dotRange = greeting.range(of: "/") {
//            greeting.removeSubrange(dotRange.lowerBound..<greeting.endIndex)
//        }
        
        
        print("Xu ly anh", greeting)
        collectionView.register(MasterCollectionViewCell.nib(), forCellWithReuseIdentifier: MasterCollectionViewCell.identifier())
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(ViewController.draggedView(_:)))
        changeImageView.isUserInteractionEnabled = true
        changeImageView.addGestureRecognizer(panGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // if checkCallAPi == false {
            
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        frameImageView.image = nil
    }
    
    func callAPIListImage() {
        getListCocktail(completion: { data in
            if let drinks = data.drinks {
                self.listCocktail = drinks
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        })
    }
    
    @objc func draggedView(_ sender: UIPanGestureRecognizer){
        self.view.bringSubviewToFront(changeImageView)
        let translation = sender.translation(in: self.view)
        changeImageView.center = CGPoint(x: changeImageView.center.x + translation.x, y: changeImageView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
//    func downloadImage(from url: String) -> UIImage? {
//        print("Download Started")
//        var image: UIImage?
//        let url = URL(string: url)!
//        getData(from: url) { data, response, error in
//            guard let data = data, error == nil else { return }
//            print(response?.suggestedFilename ?? url.lastPathComponent)
//            print("Download Finished")
//            // always update the UI from the main thread
//            DispatchQueue.main.async() { [weak self] in
//                if let imageData = UIImage(data: data) {
//                    image = imageData
//                    self?.listImage.append(imageData)
//
//                        self?.frameImageView.image = self?.listImage[0]
//
//
//                }
//            }
//        }
//        return image
//    }
//
    func downloadImage(from url: String) -> UIImage? {
        print("Download Started")
        let url = URL(string: url)!
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
                if let a = UIImage(data: data) {
                    self?.images1 = a
                    self?.listImage.append(a)
                }
            }
        }
        return images1
    }
//
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCocktail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MasterCollectionViewCell.identifier(), for: indexPath) as! MasterCollectionViewCell
        if let link = listCocktail[indexPath.row].strDrinkThumb {
            if let image = downloadImage(from: link) {
                cell.cocktailImageView.image = image
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let a = listImage[indexPath.row]
        frameImageView.image = a
        guard let fileName = listCocktail[indexPath.row].strDrinkThumb else { return }
        var imageFile = fileName
        imageFile.removeFirst(49)
        self.notificationLabel.isHidden = false
        notificationLabel.text = "Add Frame To Image Sucess!"
        let when = DispatchTime.now() + 5
        DispatchQueue.main.asyncAfter(deadline: when){ [self] in
            self.notificationLabel.isHidden = true
            let result = saveImage(image: listImage[indexPath.row], index: indexPath.row, fileName: imageFile)
            if result {
                let vc = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
                vc.fileName = imageFile
                navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    func saveImage(image: UIImage, index: Int, fileName: String?) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
//            let count = listCocktail.count
            if let fileName = fileName {
                try data.write(to: directory.appendingPathComponent(fileName)!)
            }
//            let file = "hhihihi.png"
            print("path file", directory .absoluteString)
//            try data.write(to: directory.appendingPathComponent(file)!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    
    func getListCocktail(completion: @escaping((DataModel) -> ())) {
        let link = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita"
        let url = URL(string: link)
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let json = String(bytes: data!, encoding: .utf8) {
                print(json)
            }
            do {
                let json = try JSONDecoder().decode(DataModel.self, from: data!)
                completion(json)
            } catch (let error) {
                print(error)
            }
            
        })
        task.resume()
    }
    
    func convertViewImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: masterView.bounds.size)
        let image = renderer.image { ctx in
            masterView.drawHierarchy(in: masterView.bounds, afterScreenUpdates: true)
        }
        return image
    }
    
    @IBAction func tapSelect(_ sender: Any) {
        callAPIListImage()
//        let vc = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
//        let image = convertViewImage()
////        saveImage(image: image)
//        vc.image = image
//        navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (WIDTH_SCREEN - padding * 2 - padding * (numberOfItems - 1)) / numberOfItems
        return CGSize(width: itemSize, height: itemSize)
    }
    
    // Spacing Between Each Edge Of Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: padding, left: 0, bottom: padding, right: 0)
    }
    
    // Spacing Between Rows Of Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return padding
    }
    
    //    Spacing Between Colums Of Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return padding
    }
}
