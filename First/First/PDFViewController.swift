//
//  PDFViewController.swift
//  First
//
//  Created by Boss on 18/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewController: UIViewController {
    
    var pdfImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFile()
    }
    
    func getFile() {
        
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let docURL = documentDirectory.appendingPathComponent("myFileName.pdf")

        print("docURL", docURL)
        
        _ = try? createPDF(image: pdfImage)?.write(to: docURL, atomically: true)
        
        // Add PDFView to view controller.
        let pdfView = PDFView(frame: self.view.bounds)
        pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(pdfView)

        // Fit content in PDFView.
        pdfView.autoScales = true

        // Load Sample.pdf file from app bundle.
        let fileURL = Bundle.main.url(forResource: "myFileName", withExtension: "pdf")
//        pdfView.document = PDFDocument(url: fileURL!)
        pdfView.document = PDFDocument(url: docURL)
    }
    
    func createPDF1() {
        // Create an empty PDF document
        let pdfDocument = PDFDocument()

        // Load or create your UIImage
        let image = pdfImage

        // Create a PDF page instance from your image
        let pdfPage = PDFPage(image: image)

        // Insert the PDF page into your document
        pdfDocument.insert(pdfPage!, at: 0)

        // Get the raw data of your PDF document
        let data = pdfDocument.dataRepresentation()

        // The url to save the data to
        let url = URL(fileURLWithPath: "/Path/To/Your/PDF")

        // Save the data to the url
        try! data!.write(to: url)
    }
    
    func createPDF(image: UIImage) -> NSData? {

        let pdfData = NSMutableData()
        let pdfConsumer = CGDataConsumer(data: pdfData as CFMutableData)!

        var mediaBox = CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height)

        let pdfContext = CGContext(consumer: pdfConsumer, mediaBox: &mediaBox, nil)!

        pdfContext.beginPage(mediaBox: &mediaBox)
        pdfContext.draw(image.cgImage!, in: mediaBox)
        pdfContext.endPage()

        return pdfData
    }
    
}
