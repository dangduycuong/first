//
//  MasterCollectionViewCell.swift
//  First
//
//  Created by Boss on 12/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import UIKit

class MasterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cocktailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func identifier() -> String {
        return "MasterCollectionViewCell"
    }
    
    static func nib() -> UINib {
        return UINib(nibName: MasterCollectionViewCell.identifier(), bundle: nil)
    }
    
    override func prepareForReuse() {
        cocktailImageView.image = UIImage()
    }

}
