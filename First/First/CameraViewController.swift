//
//  CameraViewController.swift
//  First
//
//  Created by Boss on 13/05/2021.
//  Copyright © 2021 Boss. All rights reserved.
//

import UIKit
import VisionKit

struct DataImage {
    var image: UIImage
    var path: String
}




class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    
    var listDataImage = [DataImage]()
    let imagePicker =  UIImagePickerController()
    
    let WIDTH_SCREEN = UIScreen.main.bounds.width
    var numberOfItems: CGFloat = 4
    let padding: CGFloat = 1
    var width: CGFloat = 1
    var sizeOfItem = CGSize(width: 92, height: 92)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        navigationController?.navigationBar.isHidden = false
        title = "Bộ sưu tập"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func tapSelectImage(_ sender: UIButton) {
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.isUserInteractionEnabled = true
        
        let alert = UIAlertController(title: "Chọn ảnh từ", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Máy ảnh", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Bộ sưu tập", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Hủy", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            print("simulator")
            let vc = storyboard?.instantiateViewController(identifier: "MasterTabbarViewController") as! MasterTabbarViewController
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    //MARK:-- ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            if saveImage(image: pickedImage) {
                
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    //luu anh
    func saveImage(image: UIImage) -> Bool {
        //xu ly anh sau do chuyen man
        print("dcdcdcdcddddcdcdc")
        let vc = storyboard?.instantiateViewController(identifier: "MasterTabbarViewController") as! MasterTabbarViewController
        navigationController?.pushViewController(vc, animated: true)
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            
//            let count = listDataImage.count
//
//            let path = "fl\(count)\(random).png"
//            try data.write(to: directory.appendingPathComponent(path)!)
//            listDataImage.append(DataImage(image: image, path: path))
            
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    //lay anh da luu
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    @IBAction func clickShowCamera(_ sender: Any) {
        let documentCameraViewController = VNDocumentCameraViewController()
        documentCameraViewController.delegate = self
        present(documentCameraViewController, animated: true)
//        openCamera()
    }
    
    
}

extension CameraViewController: VNDocumentCameraViewControllerDelegate {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        dismiss(animated: true, completion: nil)
        print("Found \(scan.pageCount)")

        for i in 0 ..< scan.pageCount {
            let img = scan.imageOfPage(at: i)
            // ... your code here
        }
        
        let vc = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
//    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
//        print("scan xong")
//
//        let myGroup = DispatchGroup()
//        for index in 0..<scan.pageCount {
//            myGroup.enter()
//            let image = scan.imageOfPage(at: index)
////            guard let imageResize = image.resize(toWidth: 475) else { return }
////            guard let dataCom = imageResize.jpegData(compressionQuality: 0.5),
////                  let imagePdf = UIImage(data: dataCom) else {
////                return
////            }
//
////            image.detechTextFormImage { (result, error) in
////                doc.content = result
////                print(doc.content ?? "")
////                docs.append(doc)
////                myGroup.leave()
////            }
//        }
////        myGroup.notify(queue: .main) { [weak self] in
////            guard let `self` = self else {return}
////            self.viewModel.documents.accept(docs)
//////            var trip = self.viewModel.trip.value
//////            trip?.documents = docs
//////            self.viewModel.trip.accept(trip)
////            self.viewModel.pageCount.accept(docs.count)
////            self.collectionView.reloadData()
//////            self.viewModel.updateContentDocs(docs: docs)
////            self.viewModel.uploadDocuments()
////        }
//        let vc = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
//        navigationController?.pushViewController(vc, animated: true)
//    }
    
}
